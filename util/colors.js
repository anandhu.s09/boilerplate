const colors = {
  themeColor: "#5AC09B",
  themGrey: "#F7F7F7",
};

export default colors;
