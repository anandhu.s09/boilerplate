import constants from "./constants";


let guestUrl = "guest/";
export class APIUrlConstants {
  static BASE_WEB_URL = constants?.BaseURL;
  static API_URLS = {
    getGuestProducts: guestUrl + "getAllPost",

  };

  static getApiUrl(key) {
    return this.BASE_WEB_URL + this.API_URLS[key];
  }

  static getApiFullUrl(url, urlDirectParams) {
    return this.BASE_WEB_URL + url + urlDirectParams;
  }
}
