import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import {
  createStackNavigator,
  HeaderBackButton,
} from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import HomeScreen from "../screens/HomeScreen/HomeScreen";
import SignInScreen from "../screens/SignInScreen/SignInScreen";
import SignUpScreen from "../screens/SignUpScreen";
import AddProductScreen from "../screens/AddProductScreen";
import CategoryScreen from "../screens/CategoryScreen";
import ProfileScreen from "../screens/ProfileScreen";
import WishlistScreen from "../screens/WishlistScreen";
import DrawerComponent from "../common/DrawerComponent";
import images from "../assets";
import { Image, Text, View, TouchableOpacity } from "react-native";
import SplashScreen from "../screens/SplashScreen/SplashScreen";
import ProductDetails from "../screens/ProductDetails/ProductDetails";
import ProductDetailHeaderRight from "../common/ProductDetailHeaderRight/ProductDetailHeaderRight";
import { useNavigation } from "@react-navigation/native";
import constants from "../util/constants";
import colors from "../util/colors";
import ForgotPassword from "../screens/ForgotPassword/ForgotPassword";
import Verification from "../screens/Verification/Verification";
import ResetPassword from "../screens/ResetPassword/ResetPassword";
import LoginContext from "../util/LoginContext";
import ProductListing from "../screens/ProductListing/ProductListing";
import ChangePassword from "../screens/ChangePassword/ChangePassword";
import Editprofile from "../screens/Editprofile/Editprofile";
import SearchScreen from "../screens/SearchScreen/SearchScreen";
import EditProduct from "../screens/EditProduct/EditProduct";
import NotificationScreen from "../screens/NotificationScreen/NotificationScreen";
import RegistrationOTP from "../screens/RegistrationOTP";
import AboutUs from "../screens/AboutUs/AboutUs";
import PrivacyPolicy from "../screens/PrivacyPolicy/PrivacyPolicy";
import Terms from "../screens/Terms/Terms";

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator>
    <AuthStack.Screen
      name="Signin"
      component={SignInScreen}
      options={{
        headerTitle: "Login",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
    <AuthStack.Screen
      name="Signup"
      component={SignUpScreen}
      options={{
        headerTitle: "Register",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
    <AuthStack.Screen
      name="Forgot"
      component={ForgotPassword}
      options={{
        headerTitle: "Forgot Password",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
    <AuthStack.Screen
      name="Verification"
      component={Verification}
      options={{
        headerTitle: "Verification",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
    <AuthStack.Screen
      name="ResetPassword"
      component={ResetPassword}
      options={{
        headerTitle: "Reset Password",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
    <AuthStack.Screen
      name="RegistrationOTP"
      component={RegistrationOTP}
      options={{
        headerTitle: "Verification",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
  </AuthStack.Navigator>
);

const HomeStack = createStackNavigator();
const HomeStackScreen = () => {
  const navigation = useNavigation();
  return (
    <HomeStack.Navigator
      tabBarOptions={{
        keyboardHidesTabBar: true,
      }}
    >
      <HomeStack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          header: () => <View>{/* <Text>lorem</Text> */}</View>,
        }}
      />
      <HomeStack.Screen
        name="ProductDetails"
        component={ProductDetails}
        options={{
          // headerTitle: "Product Details",
          headerTitleAlign: "center",
          // headerRight: () => (
          //   <ProductDetailHeaderRight navigation={navigation} />
          // ),
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
          headerTitleStyle: {
            fontWeight: "bold",
            fontFamily: "SegoeUi",
            color: "#5AC09B",
            fontSize: 21,
          },
        }}
      />
      <HomeStack.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{
          headerTitle: "Search",
          headerTitleAlign: "center",
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
          headerTitleStyle: {
            fontWeight: "bold",
            fontFamily: "SegoeUi",
            color: "#5AC09B",
            fontSize: 21,
          },
        }}
      />
      <HomeStack.Screen
        name="NotificationScreen"
        component={NotificationScreen}
        options={{
          headerTitle: "Notification",
          headerTitleAlign: "center",
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
          headerTitleStyle: {
            fontWeight: "bold",
            fontFamily: "SegoeUi",
            color: "#5AC09B",
            fontSize: 21,
          },
        }}
      />
    </HomeStack.Navigator>
  );
};

const CategoryStack = createStackNavigator();
const CategoryStackScreen = () => {
  const navigation = useNavigation();
  return (
    <CategoryStack.Navigator>
      <CategoryStack.Screen
        name="Category"
        component={CategoryScreen}
        options={{
          headerTitle: "Categories",
          headerTitleAlign: "center",
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
          headerTitleStyle: {
            fontWeight: "bold",
            fontFamily: "SegoeUi",
            color: "#5AC09B",
            fontSize: 21,
          },
          headerLeft: () => (
            <TouchableOpacity
              activeOpacity={constants.ACTIVE_OPACITY}
              onPress={() => navigation.toggleDrawer()}
            >
              <Image
                source={images.menu1}
                style={{
                  height: 28,
                  width: 28,
                  marginLeft: 12,
                  tintColor: "#5AC09B",
                }}
              />
            </TouchableOpacity>
          ),
        }}
      />
      <CategoryStack.Screen
        name="ProductListing"
        component={ProductListing}
        options={{
          headerTitle: "",
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        }}
      />
      <CategoryStack.Screen
        name="ProductDetails"
        component={ProductDetails}
        options={{
          headerTitle: "Product Details",
          headerTitleAlign: "center",
          headerRight: () => <ProductDetailHeaderRight />,
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
          headerTitleStyle: {
            fontWeight: "bold",
            fontFamily: "SegoeUi",
            color: "#5AC09B",
            fontSize: 21,
          },
        }}
      />
    </CategoryStack.Navigator>
  );
};
const AddProductStack = createStackNavigator();
const AddProductStackScreen = (props) => {
  const navigation = useNavigation();
  return (
    <AddProductStack.Navigator>
      <AddProductStack.Screen
        name="Add Product"
        component={AddProductScreen}
        options={{
          headerTitle: "Add Product",
          headerTitleAlign: "center",
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
          headerTitleStyle: {
            fontWeight: "bold",
            fontFamily: "SegoeUi",
            color: "#5AC09B",
            fontSize: 21,
          },
          headerLeft: (props) => (
            <HeaderBackButton
              {...props}
              onPress={() => {
                navigation.goBack();
              }}
            />
          ),
        }}
      />
    </AddProductStack.Navigator>
  );
};
const ProfileStack = createStackNavigator();
const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen
      name="Profile"
      component={ProfileScreen}
      options={{
        header: () => <View>{/* <Text>lorem</Text> */}</View>,
      }}
    />
    <ProfileStack.Screen
      name="ProductDetails"
      component={ProductDetails}
      options={{
        headerTitle: "Product Details",
        headerTitleAlign: "center",
        headerRight: () => <ProductDetailHeaderRight />,
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />

    <ProfileStack.Screen
      name="ChangePassword"
      component={ChangePassword}
      options={{
        headerTitle: "Change Password",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
    <ProfileStack.Screen
      name="EditProfile"
      component={Editprofile}
      options={{
        headerTitle: "Edit Profile",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
    <ProfileStack.Screen
      name="Edit Product"
      component={EditProduct}
      options={{
        headerTitle: "Edit Product",
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
        headerTitleStyle: {
          fontWeight: "bold",
          fontFamily: "SegoeUi",
          color: "#5AC09B",
          fontSize: 21,
        },
      }}
    />
  </ProfileStack.Navigator>
);
const WishlistStack = createStackNavigator();
const WishlistStackScreen = () => {
  const navigation = useNavigation();
  return (
    <WishlistStack.Navigator>
      <WishlistStack.Screen
        name="Wishlist"
        component={WishlistScreen}
        options={{
          headerTitle: "Wishlist",
          headerTitleAlign: "center",
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
          headerTitleStyle: {
            fontWeight: "bold",
            fontFamily: "SegoeUi",
            color: "#5AC09B",
            fontSize: 21,
          },
          headerLeft: () => (
            <TouchableOpacity
              activeOpacity={constants.ACTIVE_OPACITY}
              onPress={() => navigation.toggleDrawer()}
            >
              <Image
                source={images.menu1}
                style={{
                  height: 28,
                  width: 28,
                  marginLeft: 12,
                  tintColor: "#5AC09B",
                }}
              />
            </TouchableOpacity>
          ),
        }}
      />
      <WishlistStack.Screen
        name="ProductDetails"
        component={ProductDetails}
        options={{
          headerTitle: "Product Details",
          headerTitleAlign: "center",
          headerRight: () => <ProductDetailHeaderRight />,
          headerStyle: { backgroundColor: "#F3F3F3", elevation: 0 },
          headerTitleStyle: {
            fontWeight: "bold",
            fontFamily: "SegoeUi",
            color: "#5AC09B",
            fontSize: 21,
          },
        }}
      />
    </WishlistStack.Navigator>
  );
};
const TabStack = createBottomTabNavigator();
const TabStackScreen = () => (
  <TabStack.Navigator
    initialRouteName="Home"
    tabBarOptions={{
      activeTintColor: "#5AC09B",
      inactiveTintColor: "#707070",
      style: {
        backgroundColor: "#F7F7F7",
        paddingBottom: 5,
        height: 55,
      },
      labelStyle: {
        fontSize: 12,
        fontFamily: "segoe-ui",
      },
      navigationOptions: {
        header: {
          visible: true,
        },
      },
    }}
  >
    <TabStack.Screen
      name="Home"
      component={HomeStackScreen}
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
          <Image
            source={images.home}
            style={{ tintColor: focused ? "#5AC09B" : "#707070" }}
          />
        ),
        tabBarVisible: route.state && route.state.index === 0,
      })}
    />
    <TabStack.Screen
      name="Category"
      component={CategoryStackScreen}
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
          <Image
            source={images.category}
            style={{ tintColor: focused ? "#5AC09B" : "#707070" }}
          />
        ),
        tabBarVisible: route.state && route.state.index === 0,
      })}
    />
    <TabStack.Screen
      name="AddProduct"
      component={AddProductStackScreen}
      options={({ route }) => ({
        tabBarLabel: "",
        tabBarIcon: () => (
          <Image source={images.addProduct} style={{ marginTop: 20 }} />
        ),
        tabBarVisible: false,
      })}
    />
    <TabStack.Screen
      name="Profile"
      component={ProfileStackScreen}
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
          <Image
            source={images.profile}
            style={{ tintColor: focused ? "#5AC09B" : "#707070" }}
          />
        ),
        tabBarVisible: route.state && route.state.index === 0,
      })}
    />
    <TabStack.Screen
      name="Wishlist"
      component={WishlistStackScreen}
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
          <Image
            source={images.wishlist}
            style={{ tintColor: focused ? "#5AC09B" : "#707070" }}
          />
        ),
        tabBarVisible: route.state && route.state.index === 0,
      })}
    />
  </TabStack.Navigator>
);

const DrawerStack = createDrawerNavigator();
const DrawerStackScreen = () => (
  <DrawerStack.Navigator
    drawerContent={(props) => <DrawerComponent {...props} />}
  >
    <DrawerStack.Screen name="Tabs" component={TabStackScreen} />
    <DrawerStack.Screen
      name="AboutUs"
      component={AboutUs}
      options={{
        headerTitle: "About us",
      }}
    />
    <DrawerStack.Screen
      name="PrivacyPolicy"
      component={PrivacyPolicy}
      options={{
        headerTitle: "Privacy Policy",
      }}
    />
    <DrawerStack.Screen
      name="Terms"
      component={Terms}
      options={{
        headerTitle: "Terms & Condition",
      }}
    />
  </DrawerStack.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator
    screenOptions={{
      headerShown: false,
    }}
  >
    <RootStack.Screen name="Main" component={DrawerStackScreen} />
  </RootStack.Navigator>
);
const Navigation = () => {
  const [isLoading, setLoading] = React.useState(true);
  const { isLogin } = React.useContext(LoginContext);

  React.useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }
  return (
    <NavigationContainer>
      {isLogin ? <RootStackScreen /> : <AuthStackScreen />}
    </NavigationContainer>
  );
};

export default Navigation;
