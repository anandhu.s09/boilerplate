module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    'linebreak-style': ['error', 'unix'], // changes the file to LF
  },
};
