import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllGuestProductAction,
} from "../actions/productAction";

export const useGetGuestProduct = () => {
  const dispatch = useDispatch();

  const getProduct = (num, lat, long) => {
    dispatch(getAllGuestProductAction(num, lat, long));
  };

  // Reducer states
  const result = useSelector((state) => state.productReducer.guestProductList);

  return [getProduct, result.data, result.error, result.loading];
};
