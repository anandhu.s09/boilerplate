import * as types from "./ActionsTypes";
import { ProductServices } from "../services/productService";



//  GET GUEST PRODUCTS
export const getAllGuestProductAction = (num, lat, long) => {
  return {
    actionType: types.GET_GUEST_PRODUCTS_REQUEST,
    callAPI: () => ProductServices.getAllGuestProducts(num, lat, long),
    stateObject: "guestProductList",
  };
};

