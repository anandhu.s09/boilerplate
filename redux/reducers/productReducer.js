import * as types from "../actions/ActionsTypes";
import createApiReducer from "../../util/createApiReducer";

export const initialState = {
  guestProductList: {},
};
const productReducer = createApiReducer(initialState, [
  types.GET_GUEST_PRODUCTS_REQUEST,
]);

export default productReducer;
